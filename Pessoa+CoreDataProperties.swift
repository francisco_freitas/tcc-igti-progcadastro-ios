//
//  Pessoa+CoreDataProperties.swift
//  CadastroApp
//
//  Created by Freitas on 4/16/17.
//  Copyright © 2017 Freitas. All rights reserved.
//

import Foundation
import CoreData


extension Pessoa {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pessoa> {
        return NSFetchRequest<Pessoa>(entityName: "Pessoa")
    }

    @NSManaged public var nome: String?
    @NSManaged public var cpf: String?
    @NSManaged public var dataNascimento: NSDate?
    @NSManaged public var telefone: String?

}
