//
//  PessoaCell.swift
//  CadastroApp
//
//  Created by Freitas on 4/16/17.
//  Copyright © 2017 Freitas. All rights reserved.
//

import UIKit

class PessoaCell: UITableViewCell {

    @IBOutlet weak var nome: UILabel!

    func configureCell(pessoa: Pessoa){
        nome.text = pessoa.nome
    }

}
