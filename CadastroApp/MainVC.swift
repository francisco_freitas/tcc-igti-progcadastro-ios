//
//  ViewController.swift
//  CadastroApp
//
//  Created by Freitas on 4/16/17.
//  Copyright © 2017 Freitas. All rights reserved.
//

import UIKit
import CoreData

class MainVC: UIViewController, UITableViewDelegate,UITableViewDataSource, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var controller: NSFetchedResultsController<Pessoa>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        //generateTestData()
        
        attemptFetch()
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PessoaCell", for: indexPath) as! PessoaCell
        configureCell(cell: cell, indexPath: indexPath as NSIndexPath )
        return cell
    }

    func configureCell(cell:PessoaCell, indexPath: NSIndexPath ){
        let pessoa = controller.object(at: indexPath as IndexPath)
        cell.configureCell(pessoa: pessoa)
    }
    //Captura linha selecionada
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objs = controller.fetchedObjects, objs.count > 0{
            let pessoa = objs[indexPath.row]
            performSegue(withIdentifier: "PessoaDetailsVC", sender: pessoa)
        }
    }
    //Seta atributo para edicao na view de detalhe
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PessoaDetailsVC"{
            if let destination = segue.destination as? PessoaDetailsVC{
                if let pessoa = sender as? Pessoa{
                    destination.pessoaToEdit = pessoa
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = controller.sections{
        let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = controller.sections{
            return sections.count
        }
        return 0
    }
    
    func attemptFetch(){
    
        let fetchRequest: NSFetchRequest<Pessoa> = Pessoa.fetchRequest()
        
        let dataSort = NSSortDescriptor(key: "nome", ascending: true)
        
        fetchRequest.sortDescriptors =  [dataSort]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil , cacheName: nil)
        //Escutar alteracoes
        controller.delegate = self
        
        self.controller = controller
        
        do{
            
            try controller.performFetch()
        
            
        }catch{
            let error = error as NSError
            print(error)
        }
        
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
         case.insert:
            if let indexPath = newIndexPath{
                tableView.insertRows(at: [indexPath], with: .fade )
            }
            break
         case.move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            if let indexPath = newIndexPath{
                tableView.insertRows(at: [indexPath], with: .fade )
            }
            break
         case.update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRow(at: indexPath) as! PessoaCell
                configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
            }
            break
         case.delete:
            if let indexPath = indexPath {
            tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        }
    }
    
    func generateTestData(){
        let pessoa = Pessoa(context:context)
        pessoa.nome = "Francisco"
        pessoa.cpf = "06033336640"

        pessoa.telefone = "3194271728"
        
        ad.saveContext();
    }
}

