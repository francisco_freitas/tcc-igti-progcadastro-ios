//
//  PessoaDetailsVC.swift
//  CadastroApp
//
//  Created by Freitas on 4/16/17.
//  Copyright © 2017 Freitas. All rights reserved.
//

import UIKit
import CoreData

class PessoaDetailsVC: UIViewController {

    @IBOutlet weak var nomeField: UITextField!
    
    @IBOutlet weak var cpfField: UITextField!
    
    @IBOutlet weak var telefoneField: UITextField!
    
    @IBOutlet weak var dataNascimentoDatePicker: UIDatePicker!
    
    var pessoaToEdit: Pessoa?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if pessoaToEdit != nil{
            loadDadosPessoa()
        }
    }

    func loadDadosPessoa(){
        if let pessoa = pessoaToEdit{
            nomeField.text = pessoa.nome
            cpfField.text = pessoa.cpf
            telefoneField.text = pessoa.telefone
            dataNascimentoDatePicker.date = pessoa.dataNascimento as! Date
        }
    }
    
    @IBAction func deletarPessoa(_ sender: UIBarButtonItem) {
        if pessoaToEdit != nil{
         context.delete(pessoaToEdit!)
            ad.saveContext()
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func salvarPessoa(_ sender: UIButton) {
        var pessoa: Pessoa!
        
        if  pessoaToEdit == nil{
           pessoa = Pessoa(context:context)
        }else{
           pessoa = pessoaToEdit
        }
        
        pessoa.nome = nomeField.text
        pessoa.cpf = cpfField.text
        pessoa.telefone = telefoneField.text
        pessoa.dataNascimento = dataNascimentoDatePicker.date as NSDate
        ad.saveContext()
        navigationController?.popViewController(animated: true)
    }

 

}
